package com.veeam.careers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Irina Panova
 */
public class WebPageTestDrive {
    
    public static final String PAGE = "https://careers.veeam.com/"; // Page address
    public static WebDriver driver; // Reference variable of the driver
    
    private static void setWebPage(String url) {
        driver = new ChromeDriver(); // An instance of the driver
        driver.get(url); // Go to the page
        driver.manage().window().fullscreen(); // Expand the browser to full screen
    }

 	/**
     * @param country
     */
    private static void selectContry(String country) {
        // Find country selector by id and open list of options
        driver.findElement(By.id("country-element")).click();
        try {
            // Waiting for the list to open
            Thread.sleep(3000); // 3 seconds
        } catch (InterruptedException ex) {
            Logger.getLogger(WebPageTestDrive.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Select the country by the specified parameter
        driver.findElement(By.xpath("//span[contains(text(),'" + country + "')]")).click();       
    }
    
    /**
     * @param language 
     */
    private static void selectLanguage(String language) {
        // Find language selector by id and open list of options
        driver.findElement(By.id("language")).click();
        try {
            // Waiting for the list to open
            Thread.sleep(3000); // 3 seconds
        } catch (InterruptedException ex) {
            Logger.getLogger(WebPageTestDrive.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Get a list of possible options
        List<WebElement> checkbox = driver.findElements(By.className("controls-checkbox"));

        java.util.Iterator<WebElement> i = checkbox.iterator();
        
        // Choose the value by the specified parameter
        while(i.hasNext()) {
            WebElement row = i.next();
            if (row.getText().equals(language))
                row.click();     
        } 
        // Apply the selection of language
        driver.findElement(By.className("selecter-fieldset-submit")).click();
     
        try {
            // Waiting to apply
            Thread.sleep(3000); // 3 seconds
        } catch (InterruptedException ex) {
            Logger.getLogger(WebPageTestDrive.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    private static void calculateVacancies() {
        // Get the number of vacancies and print  
        String countJobs = driver.findElement(By.className("pr0-md-down")).getText();
        String[] strArray = countJobs.split("\\s");
        System.out.println("The number of discovered vacancies is: " + strArray[0]);     
    }
         
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {       
        
        // Set the system property 
        System.setProperty("webdriver.chrome.driver", "chromedriver"); 
         
        setWebPage(PAGE);
        
        // Set the input parameters of application
        WebPage veeam = new WebPage();
        veeam.setCountry("Romania");
        veeam.setLanguage("English");
        String country = veeam.getCountry();
        String language = veeam.getLanguage();
        
        selectContry(country);
        selectLanguage(language);
        calculateVacancies();
        
        try {
            // Waiting before close the browser
            Thread.sleep(5000); // 5 seconds
        } catch (InterruptedException ex) {
            Logger.getLogger(WebPageTestDrive.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        driver.quit(); // Close the browser
    }  
}