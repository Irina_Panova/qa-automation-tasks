# Selenium WebDriver App

## Overview
This application automates user actions on the browser web - interface, using Selenium WebDriver.
The objectives are:
- Go to page https://careers.veeam.com/ and expand the browser to full screen.
- From the list of countries choose Romania, from the list of languages choose English.
- Calculate the number of vacancies corresponding to the selected parameters.

## Tools
- Google Chrome 75.0.3770.142
- Chrome Driver
- Selenium WebDriver
- Maven (pom.xml file)
```
  <dependency>
  	<groupId>org.seleniumhq.selenium</groupId>
  	<artifactId>selenium-java</artifactId>
  	<version>3.141.59</version>
  </dependency>
```
### Launch
java -jar SeleniumWebDriverApp-1.0-SNAPSHOT-jar-with-dependencies.jar